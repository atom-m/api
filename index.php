<?php

$type = $argv[1];

$list = array();

// GET API
$api = 'https://bitbucket.org/api/2.0/repositories/atom-m-' . $type; // Cсылка на API

if ($type and ($type == 'plugins' or $type == 'templates')) {
    $start = microtime(true); // Отсчет загрузки с BB API начат

    function getAPI($url) {
        global $list, $type;
        
        $data = json_decode(@file_get_contents($url));

        foreach($data->values as $i => $repo) { // Проход по репозиториям
        
            $branchs = json_decode(@file_get_contents("https://bitbucket.org/api/2.0/repositories/".$repo->full_name."/refs/branches"));
            foreach($branchs->values as $k => $branch) { // Проход по веткам
                if ($branch->name == "master") {
                    continue;
                }
                
                $file = @file_get_contents('https://bitbucket.org/api/1.0/repositories/' . $repo->full_name . '/raw/'.$branch->name.'/config.json');
                if ($file) { // если конфиг существует работаем с ним.
                    $info = json_decode($file);
                
                    $pieces = explode("-", $branch->name);
                    if ((count($pieces) < 3) || (count($pieces) > 4)) {
                        continue;
                    }
                    $start = $pieces[2];
                    $stop = (count($pieces) == 4) ? $pieces[3] : $start;
                    
                    for ($version = $start; $version <= $stop; $version++) { // проход по версиям CMS
                    

                            $merge = array();
                            
                            $merge['name'] = $repo->name;

                            echo $merge['name'] . ' for v'.$version.' (branch '.$branch->name.')'.PHP_EOL; // выводим название плагина

                            if (isset($info->title)) $merge['title'] = $info->title;
                            if (isset($info->desc)) $merge['desc'] = $info->desc;
                            if (isset($info->icon)) $merge['icon'] = $info->icon;
                            if (isset($info->more)) $merge['more'] = $info->more;
                            $merge['version'] = $branch->target->hash;
                            $merge['path_raw'] = 'https://bitbucket.org/api/1.0/repositories/atom-m-' . $type . '/'.$merge['name'].'/raw/'.$branch->name.'/';
                            $merge['path_zip'] = 'https://bitbucket.org/atom-m-' . $type . '/'.$merge['name'].'/get/'.$branch->name.'.zip';
                            
                            if (!isset($list[$version])) {
                                $list[$version] = array();
                            }
                            
                            $list[$version][] = $merge;


                    }
                    
                    
                }
            }
            echo PHP_EOL;
            
        }
        
        if (isset($data->next)) // Если есть ссылка на следующую страницу
            getAPI($data->next); // грузим её
        else // если нету, выводим данные в файлы
        
            foreach($list as $i => $version) {
                file_put_contents('/home/admin/web/api.atom-m.net/public_html/' . $type . '-'.$i.'.json', json_encode($version, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            }
    }

    getAPI($api); // начать загрузку api

    echo 'Данные API успешно обновлены. Время выполнения: '.(microtime(true) - $start).' сек.';
} else
    echo 'Некорректный запрос';
?>
